/*
 * File:   main.c
 * Author: abdullah
 *
 * Created on 30 Mart 2013 Cumartesi, 13:29
 */

#include <xc.h> // Include the compiler's header file.
//This definition is needed for using __delay(int x):
#define _XTAL_FREQ 4000000 // We are using the 4MHz Internal Oscillator.

/******* Configuration Word ********/
__CONFIG(FOSC_INTRCIO & WDTE_OFF & PWRTE_ON & MCLRE_OFF & BOREN_ON & CP_ON & CPD_ON);
//INTOSC oscillator: I/O function on GP4/OSC2/CLKOUT pin, I/O function on GP5/OSC1/CLKIN
// WDT disabled
// PWRT enabled
// GP3/MCLR pin function is digital input, MCLR internally tied to VDD
// Brown-out Detect Enabled
// Program Memory code protection is enabled
// Data memory code protection is enabled
/**** End of Configuration Word ****/

/****** Input Output Definitions *******/
#define Fan_Transistor          GPIObits.GP0   // Controls a transistor that drives a fan.
#define TRIS_Fan_Transistor     TRISIO0        // Tri-state control bit
/*** End Of Input Output Definitions ***/

/****** ADC Channel Definitions *******/
// Shows analog channel connections. For example, "3" shows that it's connected to AN3
#define NTC_AnalogChannel   2 // There is an NTC thermistor connected to this channel.
#define TRISNTC             TRISIO2 // Tri-state control bit
/*** End Of ADC Channel Definitions ***/

unsigned int temperatureReading;

void ADC_Sampler(void)
{
    // temp in degC = 830 - 1.025*ADC_Result
    ADON = 1; // Turn the ADC ON
    ADCON0bits.CHS = NTC_AnalogChannel; // Select the appropriate analog channel (Max)
    __delay_us(50); // Wait for 50us ADC acquisition time.
    GO_nDONE = 1; // Start the conversion.
    while (GO_nDONE); // Wait until the conversation is done.

    temperatureReading = ADRESL + (unsigned int) (ADRESH << 8); // Combine 8 bit "ADRESH" and  8 bit "ADRESL" to one 16 bit register.
    ADON = 0; // Turn the ADC OFF.
}

void main()
{
    ADCON0 = 0x80; // ADC result is right justified.
    ANSEL = 0x14; // Disable all the analog inputs, except for NTC. ADC clock is FOSC/8
    CMCONbits.CM = 0x07; // CM2:CM0: Comparator Mode bits: 111 = Comparator OFF
    TRIS_Fan_Transistor = 0; // Set the port as output
    Fan_Transistor = 0; // and clear it.
    TRISNTC = 1; // Set the port as input.

    while (1)
    {
        ADC_Sampler(); // Call the ADC sampler.
        if (temperatureReading >= 575)
        {
            Fan_Transistor = 1;
        }
        else if (temperatureReading <= 530)
        {
            Fan_Transistor = 0;
        }
    }
}
